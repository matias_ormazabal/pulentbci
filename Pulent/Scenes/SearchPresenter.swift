import UIKit

protocol SearchPresentationLogic{
  func presentSearchMusic(response: Search.SearchMusic.Response)
}

class SearchPresenter: SearchPresentationLogic{
  weak var viewController: SearchDisplayLogic?
  
  func presentSearchMusic(response: Search.SearchMusic.Response){
    let viewModel = Search.SearchMusic.ViewModel(itunesResult: response.itunesResult)
    viewController?.displaySomething(viewModel: viewModel)
  }
}
