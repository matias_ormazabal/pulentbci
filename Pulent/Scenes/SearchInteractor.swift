import UIKit

protocol SearchBusinessLogic{
  func doSearchMusic(request: Search.SearchMusic.Request)
}

protocol SearchDataStore{
  //var name: String { get set }
}

class SearchInteractor: SearchBusinessLogic, SearchDataStore{
  var presenter: SearchPresentationLogic?
  var worker: SearchWorker?
    
  func doSearchMusic(request: Search.SearchMusic.Request){
    worker = SearchWorker()
  
    worker?.getSearch(withName: request.nameMusic,
                      withLimit: request.limit,
                      withOffset: request.offset,
                      completionHandler: { result in
        let response = Search.SearchMusic.Response(itunesResult: result)
        self.presenter?.presentSearchMusic(response: response)
        
    })
 
    
  }
}
