import UIKit

enum Search{
  enum SearchMusic{
    
    struct Request{
        let nameMusic : String
        let limit : Int
        let offset : Int
    }
    
    struct Response{
        let itunesResult : ItunesResult
    }
    
    struct ViewModel{
        let itunesResult : ItunesResult
    }
  }
}
