import UIKit
import JGProgressHUD
import AVFoundation

protocol SearchDisplayLogic: class{
    func displaySomething(viewModel: Search.SearchMusic.ViewModel)
}

class SearchViewController: UIViewController, SearchDisplayLogic{
    var interactor: SearchBusinessLogic?
    var router: (NSObjectProtocol & SearchRoutingLogic & SearchDataPassing)?
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private var searchBar: UISearchBar?
    var searchBarValue:String!
    var searchActive : Bool = false
    let hud = JGProgressHUD(style: .dark)
    var resultsArray = [ResultElement]()
    
    private var actualOffset = 0
    private var actualLimit = 20
    
    private var player: AVPlayer!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup(){
        let viewController = self
        let interactor = SearchInteractor()
        let presenter = SearchPresenter()
        let router = SearchRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.searchBar?.delegate = self
    }
    
    func doSearchMusic(){
        self.hud.textLabel.text = "Buscando..."
        self.hud.show(in: self.view)
        let request = Search.SearchMusic.Request(nameMusic: searchBarValue, limit: actualLimit, offset: actualOffset)
        interactor?.doSearchMusic(request: request)
    }
    
    func displaySomething(viewModel: Search.SearchMusic.ViewModel){
        
        resultsArray = viewModel.itunesResult.results
        print(resultsArray.count)
        if resultsArray.count == 0{
            print("no hay resultados")
            DispatchQueue.main.async {
                self.hud.dismiss()
                self.tableView?.reloadData()
                self.showMessage(withMessage: "No hay resultados :(")
            }
        }else{
            DispatchQueue.main.async {
                self.tableView?.reloadData()
                self.hud.dismiss(afterDelay: 0.5)
            }
        }
    }
    
    func showMessage(withMessage: String){
        let alert = UIAlertController(title: "Info!", message: withMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showHudAutoDismiss(withMessage: String){
        self.hud.textLabel.text = withMessage
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 0.5)
    }
    
    func playMusic(url: String) {
        let url  = URL.init(string:  url)
        
        if self.player != nil{
            self.player.seek(to: CMTime.zero)
            self.player.pause()
        }

        let playerItem: AVPlayerItem = AVPlayerItem(url: url!)
        self.player = AVPlayer(playerItem: playerItem)
        let playerLayer = AVPlayerLayer(player: player!)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 50)
        self.view.layer.addSublayer(playerLayer)
        self.player.play()
    }
    
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let song = resultsArray[indexPath.row]
        showHudAutoDismiss(withMessage: "Reproduciendo...")
        self.playMusic(url: song.previewUrl!)
        self.tableView?.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ResultCell.cellIdentifier, for: indexPath) as! ResultCell
        let song = resultsArray[indexPath.row]
        cell.setData(song: song)
        
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == tableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
                self.actualLimit+=20
                doSearchMusic()
            }
        }
    }
}


extension SearchViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        self.searchBar?.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        searchBar.resignFirstResponder()
        self.tableView?.resignFirstResponder()
        self.searchBar?.showsCancelButton = false
        self.tableView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.searchBarValue = self.searchBar?.text;
        self.searchBar?.resignFirstResponder()
        self.tableView?.resignFirstResponder()
        self.actualOffset = 0
        self.actualLimit = 20
        doSearchMusic()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchActive = true;
        self.searchBar?.showsCancelButton = true
    }
    
}
