import UIKit
 
class ResultCell : UITableViewCell{
    
    static let  cellIdentifier = "songCell"
    
    @IBOutlet weak var artistLabel: UILabel?
    @IBOutlet weak var songNameLabel: UILabel?
    @IBOutlet weak var artworkImage: UIImageView?
    
    func setData(song: ResultElement){
        artistLabel?.text = song.artistName
        songNameLabel?.text = song.trackName
        artworkImage?.imageFromServerURL(urlString: song.artworkUrl100!)
    }
    
}
