import UIKit

class SearchWorker{
    func getSearch(withName: String, withLimit: Int, withOffset: Int,completionHandler: @escaping (ItunesResult) -> Void){
        ApiServices.shared.searchMusic(withName: withName, withLimit: withLimit, withOffset: withOffset, completionHandler: { itunesResult in
            completionHandler(itunesResult)
        })
    }
}
