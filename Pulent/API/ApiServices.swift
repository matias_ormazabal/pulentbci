//
//  ApiServices.swift
//  Pulent
//
//  Created by Matías Ormazabal on 21-01-20.
//  Copyright © 2020 Matías Ormazabal. All rights reserved.
//

import Foundation

class ApiServices {
    static let shared = ApiServices()
    private let baseURL = "https://itunes.apple.com/search?mediaType=music"
    

    
    private init() {}
    
    func searchMusic(withName:String, withLimit: Int, withOffset: Int, completionHandler: @escaping (ItunesResult) -> Void ) {
        let nameMusic = withName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
        
        let formatUrl = String(format: "&limit=%d&offset=%d&term=%@", withLimit,withOffset,nameMusic)
        
        let url = URL(string: baseURL + formatUrl)!
       
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            DispatchQueue.main.async {
                if let data = data {
                    let decoder = JSONDecoder()
                    do {
                        let decodedPosts = try decoder.decode(ItunesResult.self, from: data)
                      
                        completionHandler(decodedPosts)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
}
